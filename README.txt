Sometimes the batch operation provided by Drupal is not enough to operate on a
long list of users, for example if you want to delete thousands users in one go.

This module provides a way to activate, block or delete users based on uid.
It implements with batch API so you don't need to worry about timeout,
simply copy and paste a list of uid, then you are good to go.
