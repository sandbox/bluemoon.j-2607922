<?php
/**
 * @file
 * batch_user_operations.module
 */

/**
 * Implements hook_menu().
 */
function batch_user_operations_menu() {
  $items[] = array();

  $items['admin/people/batch_user_operations'] = array(
    'title' => "Batch User Operations",
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('batch_user_operations_action_form'),
    'access arguments' => array('administer users'),
  );

  return $items;
}


/**
 * Implements hook_form().
 */
function batch_user_operations_action_form() {
  $form = array();

  $form['#attributes'] = array('enctype' => "multipart/form-data");

  $form['batch_user_operations_action'] = array(
    '#type' => 'radios',
    '#required' => TRUE,
    '#title' => t('Action'),
    '#default_value' => 'block',
    '#options' => array(
      'block' => t('Block users'),
      'delete' => t('Delete users'),
      'activate' => t('Activate users'),
    ),
  );

  $form['batch_user_operations_uid_list'] = array(
    '#type' => 'textarea',
    '#title' => t('List of user ID'),
    '#description' => t("Separate user IDs with comma. For example 1,2,3"),
  );
  $form['submit_upload'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function batch_user_operations_action_form_validate($form, &$form_state) {
  $val = trim($form_state['values']['batch_user_operations_uid_list']);
  $ids = explode(',', $val);

  $action = $form_state['values']['batch_user_operations_action'];

  if (!in_array($action, array('block', 'delete', 'activate'))) {
    form_set_error('batch_user_operations_action', t("Invalid action type."));
  }

  foreach ($ids as $uid) {
    if (!is_numeric($uid)) {
      form_set_error('batch_user_operations_uid_list', t("Contains invalid user ID. User ID must be integer: %uid", array('%uid' => $uid)));
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function batch_user_operations_action_form_submit($form, &$form_state) {
  $val = trim($form_state['values']['batch_user_operations_uid_list']);
  $ids = explode(',', $val);
  $action = $form_state['values']['batch_user_operations_action'];

  $batch = array(
    'title' => t('@action member profiles', array('action' => $action)),
    'operations' => array(),
    'finished' => '_batch_user_operations_action_finished',
    'init_message' => t('Parsing User IDs'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Processing has encountered an error.'),
  );

  $counts = count($ids);
  foreach ($ids as $uid) {
    $uid = trim($uid);
    $batch['operations'][] = array('_batch_user_operations_action_single_process',
      array($uid,
        $counts,
        $form_state['values']['batch_user_operations_action'],
      ),
    );
  }
  // Start the batch.
  batch_set($batch);
}

/**
 * Single process.
 */
function _batch_user_operations_action_single_process($uid, $max, $action, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = $max;
  }

  $context['sandbox']['progress']++;
  $context['sandbox']['current_node'] = "uid: " . $uid;
  $context['message'] = "uid: " . $uid;

  $ok = NULL;
  if ($action == 'block') {
    $account = user_load($uid);
    $edit = array('status' => 0);
    if ($account) {
      $ok = user_save($account, $edit);
    }
  }
  elseif ($action == 'activate') {
    $account = user_load($uid);
    $edit = array('status' => 1);
    if ($account) {
      $ok = user_save($account, $edit);
    }
  }
  elseif ($action == 'delete') {
    user_delete($uid);
    $ok = TRUE;
  }

  if ($ok) {
    $context['results']['success'][] = $uid;
  }
  else {
    $context['results']['fail'][] = $uid;
  }
}

/**
 * Callback funciton after finishing.
 */
function _batch_user_operations_action_finished($success, $results, $operations) {
  if ($success) {
    $message = t('@count users have been updated.', array("@count" => count($results['success'])));
  }
  else {
    $message = t('Finished with error');
  }

  drupal_set_message($message);
}
